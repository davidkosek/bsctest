package cz.bsc.java.test;

import org.junit.Test;
import cz.bsc.java.test.model.Payment;
import cz.bsc.java.test.utils.CommonHelper;
import junit.framework.TestCase;

/**
 * 
 * Some unit test for test app BSC.
 *
 * @author: David Kosek
 * @since: 24. 2. 2018
 */
public class CommonHelperTest extends TestCase {

	@Test
	public void testConvertValidRates() {
		final String USDrates = "20.32";
		final double rates = CommonHelper.checkAndConvertRates(USDrates);
		assertEquals(20.32, rates);
	}

	@Test
	public void testConvertWrongRates() {
		final String USDrates = "-20.32";
		final double rates = CommonHelper.checkAndConvertRates(USDrates);
		assertEquals(0.0, rates);
	}

	@Test
	public void testConvertNoNumberRates() {
		final String USDrates = "abc";
		final double rates = CommonHelper.checkAndConvertRates(USDrates);
		assertEquals(0.0, rates);
	}

	@Test
	public void testConvertNoNumberTime() {
		final String timeInterval = "abc";
		final long interval = CommonHelper.checkAndConvertTimePeriod(timeInterval);
		assertEquals(60000, interval);
	}

	@Test
	public void testConvertNoValidTime() {
		final String timeInterval = "-123";
		final long interval = CommonHelper.checkAndConvertTimePeriod(timeInterval);
		assertEquals(60000, interval);
	}

	@Test
	public void testConvertValidTime() {
		final String timeInterval = "1000";
		final long interval = CommonHelper.checkAndConvertTimePeriod(timeInterval);
		assertEquals(1000, interval);
	}

	@Test
	public void testParseValidLine() {
		final String line = "CZK 1000";
		final Payment payment = CommonHelper.parseLineToPayment(line);
		assertEquals("CZK", payment.getCode());
		assertEquals(1000, payment.getAmount());
	}

	@Test
	public void testParseNoValidLine() {
		final String line = "CZK abc";
		final Payment payment = CommonHelper.parseLineToPayment(line);
		assertEquals(0, payment.getAmount());
	}

	@Test
	public void testValidLineFormat() {
		final String line = "CZK 1000";
		final boolean lineFormat = CommonHelper.checkLineFormat(line);
		assertTrue(lineFormat);
	}

	@Test
	public void testNoValidLineFormat() {
		final String line = "CzK 1000";
		final boolean lineFormat = CommonHelper.checkLineFormat(line);
		assertFalse(lineFormat);
	}
}
