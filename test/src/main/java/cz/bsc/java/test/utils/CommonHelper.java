package cz.bsc.java.test.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import cz.bsc.java.test.model.Payment;

/**
 * 
 * Common methods for read files, check values, etc.
 *
 * @author: David Kosek
 * @since: 24. 2. 2018
 */
public class CommonHelper {

	private static final String LINE_FORMAT = "^([A-Z]{3})\\s[-]?\\d+";
	private static final long DEFAULT_TIME_PERIOD = 60000;

	/**
	 * Read files and save values to tracker.
	 * 
	 * @param aArgs arguments with names of files
	 * @param aPaymentsTrackerList list of payments tracker
	 */
	public static void readFile(final String[] aArgs, final List<Payment> aPaymentsTrackerList) {

		BufferedReader br = null;
		// try to get data from all of files
		for (int i = 0; i < aArgs.length; i++) {
			try {
				// try to read file
				br = new BufferedReader(new InputStreamReader(ClassLoader.getSystemClassLoader().getResourceAsStream("resources/" + aArgs[i])));
				// this case isnt work in runnable jar
				// br = Files.newBufferedReader(Paths.get(ClassLoader.getSystemResource(aArgs[i]).toURI()));

				// read, check lines and save to paymentsTrackerList
				br.lines().filter(line -> checkLineFormat(line)).forEach(line -> {
					// synchronized access
					synchronized (aPaymentsTrackerList) {
						// create new record
						final Payment payment = parseLineToPayment(line);
						// added to list
						aPaymentsTrackerList.add(payment);
					}
				});
			} catch (final Exception e) {
				final String errMsg = String.format("File %s not found in resources directory.", aArgs[i]);
				System.err.println(errMsg);
			} finally {
				if (br != null) {
					// close file
					try {
						br.close();
					} catch (final IOException e) {
						final String errMsg = String.format("Error occured during close file %s.", aArgs[i]);
						System.err.println(errMsg);
					}
				}
			}
		}
	}

	/**
	 * Check line format by defined pattern.
	 * 
	 * @param aLine line to check
	 * @return true when line is correct otherwise false
	 */
	public static boolean checkLineFormat(final String aLine) {
		return aLine.matches(LINE_FORMAT);
	}

	/**
	 * Convert line to payment object.
	 * 
	 * @param aLine line to convert
	 * @return payment record
	 */
	public static Payment parseLineToPayment(final String aLine) {
		final Payment payment = new Payment();
		{
			try {
				final String key = aLine.substring(0, 3);
				final int a = Integer.valueOf(aLine.substring(4, aLine.length()));
				payment.setCode(key);
				payment.setAmount(a);
			} catch (final Exception e) {
				final String errMsg = String.format("Error occrured during parse line to payment %s.", aLine);
				System.err.println(errMsg);
			}
		}
		return payment;
	}

	/**
	 * Convert time interval to number.
	 * 
	 * @param aTimePeriodToPrint time to convert
	 * @return time interval as number
	 */
	public static long checkAndConvertTimePeriod(final String aTimePeriodToPrint) {
		if (aTimePeriodToPrint == null) {
			return DEFAULT_TIME_PERIOD;
		}
		try {
			final long timePeriod = Long.parseLong(aTimePeriodToPrint);
			// rather check it
			return timePeriod > 0 ? timePeriod : DEFAULT_TIME_PERIOD;
		} catch (final NumberFormatException e) {
			// can be define default
			System.err.println("Error occured during convert time period to number -> will be use default value 60s.");
			return DEFAULT_TIME_PERIOD;
		}
	}

	/**
	 * Convert rates to number.
	 * 
	 * @param aUSDrates rates to convert
	 * @return rates as number
	 */
	public static double checkAndConvertRates(final String aUSDrates) {
		if (aUSDrates == null) {
			return 0;
		}
		try {
			final double rates = Double.parseDouble(aUSDrates);
			// rather check it
			return rates > 0 ? rates : 0;
		} catch (final NumberFormatException e) {
			// can be define default
			System.err.println("Error occured during convert rates to number -> will not be used.");
			return 0;
		}
	}

}
