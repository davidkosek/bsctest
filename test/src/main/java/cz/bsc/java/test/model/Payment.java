package cz.bsc.java.test.model;

/**
 * 
 * Payment basic model.
 *
 * @author: David Kosek
 * @since: 24. 2. 2018
 */
public class Payment {

	private String code;
	private int amount;

	public String getCode() {
		return this.code;
	}

	public void setCode(final String aCode) {
		this.code = aCode;
	}

	public int getAmount() {
		return this.amount;
	}

	public void setAmount(final int aAmount) {
		this.amount = aAmount;
	}

}
