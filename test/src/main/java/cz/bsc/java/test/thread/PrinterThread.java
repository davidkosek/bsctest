package cz.bsc.java.test.thread;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Properties;
import cz.bsc.java.test.model.Payment;
import cz.bsc.java.test.utils.CommonHelper;

/**
 * 
 * Printer thread which in the first step loading config values and then prints a payments in the
 * defined time interval.
 *
 * @author: David Kosek
 * @since: 24. 2. 2018
 */
public class PrinterThread implements Runnable {

	private List<Payment> paymentsTrackerList;
	private String timePeriodToPrint = null;
	private String USDrates = null;

	public PrinterThread(final List<Payment> aPaymentsTrackerList) {
		this.paymentsTrackerList = aPaymentsTrackerList;
	}

	@Override
	public void run() {
		// loading config values
		loadConfigValues();

		// convert string to number format
		final long timePeriodToPrintConverted = CommonHelper.checkAndConvertTimePeriod(this.timePeriodToPrint);
		final double USDratesConverted = CommonHelper.checkAndConvertRates(this.USDrates);

		// start print
		managePrinter(timePeriodToPrintConverted, USDratesConverted);
	}

	/**
	 * Loading config properties.
	 */
	private void loadConfigValues() {
		System.out.println("Try to load config file.");
		BufferedReader br = null;
		try {
			// load a properties file
			final Properties prop = new Properties();
			// try to read file
			br = new BufferedReader(new InputStreamReader(ClassLoader.getSystemClassLoader().getResourceAsStream("resources/config.properties")));
			prop.load(br);

			// get the property value
			this.timePeriodToPrint = prop.getProperty("timePeriodToPrint");
			this.USDrates = prop.getProperty("USDrates");
			System.out.println("Config file is loaded.");
		} catch (final IOException e) {
			System.err.println("Error occured during reading config file.");
			// when failed reading config dont remember close thread
			Thread.interrupted();
		} catch (final NullPointerException e) {
			System.err.println("Config file not found.");
			this.timePeriodToPrint = null;
			this.USDrates = null;
		} finally {
			if (br != null) {
				// close file
				try {
					br.close();
				} catch (final IOException e) {
					System.err.println("Error occured during close file config.properties.");
				}
			}
		}

	}

	/**
	 * Print a payments in defined time interval.
	 * 
	 * @param aTimePeriodToPrintConverted defined time interval
	 * @param aUSDratesConverted rates to USDs
	 */
	private void managePrinter(final long aTimePeriodToPrintConverted, final double aUSDratesConverted) {

		System.out.println("Printer thread is ready...");

		// will be closed by "main" thread when users finished inputstream
		while (!Thread.interrupted()) {
			try {
				Thread.sleep(aTimePeriodToPrintConverted);
			} catch (final InterruptedException e) {
				// thread is interrupted -> break loop
				break;
			}

			System.out.println("--------------------------");
			System.out.println("Print payments transaction");
			System.out.println("--------------------------");

			synchronized (paymentsTrackerList) {
				paymentsTrackerList.stream().filter(payment -> payment.getAmount() != 0).forEach(payment -> {
					final int amount = payment.getAmount();
					final String code = payment.getCode();
					String msg;
					// recalculate different currency than USD
					if (code.equals("USD") || aUSDratesConverted == 0) {
						msg = String.format("%s %s", code, amount);
					} else {
						msg = String.format("%s %s (USD %.2f)", code, amount, amount / aUSDratesConverted);
					}
					System.out.println(msg);
				});
			}
		}
		System.out.println("Printer thread is closed...");

	}

}
