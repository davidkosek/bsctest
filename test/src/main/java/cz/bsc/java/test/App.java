package cz.bsc.java.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import cz.bsc.java.test.model.Payment;
import cz.bsc.java.test.thread.PrinterThread;
import cz.bsc.java.test.utils.CommonHelper;

/**
 * 
 * Main class which start a child thread for prints payments and reads users inputstream from
 * console.
 *
 * @author: David Kosek
 * @since: 24. 2. 2018
 */
public class App {

	/** Keyword for finish users inputstream. */
	private static final String EXIT = "quit";

	public static void main(final String[] args) {

		// create payments tracker
		final List<Payment> paymentsTrackerList = Collections.synchronizedList(new ArrayList<>());

		// exists file on start -> read, check and save information
		if (args != null && args.length >= 1) {
			CommonHelper.readFile(args, paymentsTrackerList);
		}

		// create and start thread on back for printer payments
		final PrinterThread printThread = new PrinterThread(paymentsTrackerList);
		final Thread printerThread = new Thread(printThread);
		printerThread.start();

		// no need to start other threads - we use "main" thread for reading input
		readUsersInput(paymentsTrackerList, printerThread);

		while (printerThread.isAlive()) {
			// waiting for close print thread - only cosmetics
		}
		System.out.println("All thread is closed...");
	}

	/**
	 * Loop for reading users inputstream and added to tracker list.
	 * 
	 * @param aPaymentsTrackerList tracker list
	 * @param aPrinterThread printer thread
	 */
	private static void readUsersInput(final List<Payment> aPaymentsTrackerList, final Thread aPrinterThread) {

		// declaration inputstream
		final BufferedReader into = new BufferedReader(new InputStreamReader(System.in));
		boolean inputLoop = true;
		String line;
		System.out.println("Inputstream is ready...");

		// start loop
		while (inputLoop) {
			try {
				// get next line
				line = into.readLine();

				// when line isnt empty
				if (line != null && !line.isEmpty()) {

					// break looop
					if (line.equalsIgnoreCase(EXIT)) {
						inputLoop = false;
						// close printer thread, can be closed yet so check it rather
						if (aPrinterThread.isAlive()) {
							aPrinterThread.interrupt();
						}
						break;
					}

					// check format and add or skip
					if (CommonHelper.checkLineFormat(line)) {
						synchronized (aPaymentsTrackerList) {
							final Payment payment = CommonHelper.parseLineToPayment(line);
							// added to list
							aPaymentsTrackerList.add(payment);
							System.out.println("Payment added to tracker list.");
						}
					} else {
						final String errMsg = String.format("Line '%s' is incorrect. Correct format is for example 'USD 1000'.", line);
						System.err.println(errMsg);
					}
				} else {
					System.err.println("Empty line is ignored.");
				}
			} catch (final IOException e) {
				System.err.println("Error occured during reading lines from command line.");
			}
		}

		System.out.println("Inputstream is closed...");
	}

}
