﻿# BCS Java test

Entry test for BSC - Payment  tracker

## Getting Started

These instructions will get you a running on your local machine. See notes on how to run the project.

## Configuration application

You can set up a two properties for application. Config file is placed in resources (src/main/resources). This place cannot be changed without change in source code.

* timePeriodToPrint - define a time interval how often is printed records in tracker list (in milliseconds)
* USDrates - define a USD exchange rate

## Running app

You have a few options how to running app.

* You have to use command line and start jar file in project. You can use arguments as names of files for loading data (one or more files). The files must be placed in resources (src/main/resources).

```
java -jar app.jar 

java -jar app.jar test.txt

java -jar app.jar test.txt test_1.txt test_2.txt 

```
* The ohter choice is use prepared bat files.

## Author

* **David Košek** 
* kosek.david@centrum.cz